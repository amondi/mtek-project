package com.amondi.amondi;

public class ServerStatus {
    private String Group;
    private String Artifact;
    private String Version;
    private String Status;

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getArtifact() {
        return Artifact;
    }

    public void setArtifact(String artifact) {
        Artifact = artifact;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
